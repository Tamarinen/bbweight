# This code started its life at https://gist.github.com/rdb/8864666
#
# Released by rdb under the Unlicense (unlicense.org)
# Based on information from:
# https://www.kernel.org/doc/Documentation/input/joystick-api.txt

import os
import struct
import array
from fcntl import ioctl
import time
import math
import sys


class bbweight:
    def __init__(self, bbid):
        # We'll store the states here.
        self.axis_states = {}
        self.button_states = {}

        # These constants were borrowed from linux/input.h
        self.axis_names = {
            0x10: 'hat0x',
            0x11: 'hat0y',
            0x12: 'hat1x',
            0x13: 'hat1y',
        }

        self.button_names = {
            0x130: 'a',
        }

        self.axis_map = []
        self.button_map = []

        self.fn = '/dev/input/js0'
        self.bbid = bbid

    def open_board(self):
        self.jsdev = None
        # Open the joystick device.
        print('Waiting for %s...' % self.fn)
        while not self.jsdev:
            try:
                self.jsdev = open(self.fn, 'rb')
            except IOError:
                time.sleep(5)

        # Get the device name.
        # buf = bytearray(63)
        buf = array.array('B', [0] * 64)
        ioctl(self.jsdev, 0x80006a13 + (0x10000 * len(buf)),
              buf)  # JSIOCGNAME(len)
        js_name = buf.tostring()
        print('connected: Device name: %s' % js_name)

        # Get number of axes and buttons.
        buf = array.array('B', [0])
        ioctl(self.jsdev, 0x80016a11, buf)  # JSIOCGAXES
        num_axes = buf[0]

        buf = array.array('B', [0])
        ioctl(self.jsdev, 0x80016a12, buf)  # JSIOCGBUTTONS
        num_buttons = buf[0]

        # Get the axis map.
        buf = array.array('B', [0] * 0x40)
        ioctl(self.jsdev, 0x80406a32, buf)  # JSIOCGAXMAP

        for axis in buf[:num_axes]:
            axis_name = self.axis_names.get(axis, 'unknown(0x%02x)' % axis)
            self.axis_map.append(axis_name)
            self.axis_states[axis_name] = 0.0

        # Get the button map.
        buf = array.array('H', [0] * 200)
        ioctl(self.jsdev, 0x80406a34, buf)  # JSIOCGBTNMAP

        for btn in buf[:num_buttons]:
            btn_name = self.button_names.get(btn, 'unknown(0x%03x)' % btn)
            self.button_map.append(btn_name)
            self.button_states[btn_name] = 0

        print('{} axes found: {}'.format(num_axes,
                                         ', '.join(self.axis_map)))
        print('{} buttons found: {}'.format(num_buttons,
                                            ', '.join(self.button_map)))

        return self.jsdev

    def bbdisconnect(self):
        os.system("echo disconnect {}|bluetoothctl".format(self.bbid))


if len(sys.argv) < 2:
    print("error: Please supply the balance board's Bluetooth address.")
    exit(1)

# Main event loop
measure = True
bb = bbweight(sys.argv[1])
while measure:
    jsdev = bb.open_board()
    calibrate = True
    caldata = {}
    calcnt = 100
    calval = None

    weightdata = {}
    weightcnt = 100
    weightval = None

    auto_off = 20
    stepped_on = False
    weights = []

    while jsdev and measure:
        try:
            evbuf = jsdev.read(8)
        except IOError:
            print("Lost board connection")
            jsdev = None

        if evbuf and jsdev:
            evtime, value, type, number = struct.unpack('IhBB', evbuf)

            if type & 0x80:
                print("(initial)")

            if type & 0x01:
                button = bb.button_map[number]
                if button:
                    bb.button_states[button] = value
                    if value:
                        print("button: {} pressed".format(button))
                    else:
                        print("button: {} released".format(button))

            if type & 0x02:
                axis = bb.axis_map[number]
                if axis:
                    fvalue = value + 32767  # / 32767.0
                    bb.axis_states[axis] = fvalue
                    weight = (bb.axis_states["hat0x"] +
                              bb.axis_states["hat0y"] +
                              bb.axis_states["hat1x"] +
                              bb.axis_states["hat1y"]) / 100.0

                    if calibrate:
                        if calcnt:
                            caldata[calcnt] = weight
                            calcnt -= 1
                        else:
                            calval = sum(caldata.values()) / len(caldata)
                            calibrate = False
                            print("{}".format(caldata))
                            print("{}".format(calval))
                    else:
                        weightdata[weightcnt] = weight
                        weightcnt -= 1
                        if not weightcnt:
                            weightcnt = 100
                            allweights = sum(weightdata.values())
                            weightavg = allweights / len(weightdata) - calval
                            if weightavg > 10:
                                stepped_on = True
                                weights.append(weightavg)
                                print("update: {} {} {} {} {:.4}".format(
                                    bb.axis_states["hat0x"],
                                    bb.axis_states["hat0y"],
                                    bb.axis_states["hat1x"],
                                    bb.axis_states["hat1y"],
                                    weightavg))
                            else:
                                if stepped_on:
                                    bb.bbdisconnect()
                                else:
                                    print("off_in: {}".format(auto_off))
                                    auto_off -= 1
                                    if not auto_off:
                                        bb.bbdisconnect()

    if len(weights) > 3:
        onepart = math.ceil(len(weights) / 3)
        middle = sorted(weights)[onepart:-onepart]
        finalweight = sum(middle) / len(middle)
    else:
        finalweight = 0.0
    print("weight: {:.4}".format(finalweight))
